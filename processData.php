<?php

$files = scandir('./resource');

echo "======== start ========" . PHP_EOL;
foreach($files as $file) {
    if ($file == '.' || $file == '..') {
        continue;
    }
    echo './resource/' . $file . PHP_EOL;
    $file_content = file_get_contents('./resource/'. $file);
    $request_arr = explode('------------------------------------------------------------------', $file_content);
    
    $data = [];
    foreach($request_arr as $reqeust) {
        $reqeust = trim($reqeust);
        if (!$reqeust) {
            continue;
        }
        $tmp_arr = explode("\n", $reqeust);
        if (!isset($tmp_arr[0]) || !$tmp_arr[0]) {
            continue;
        }
        $data[] = [
            'url' => $tmp_arr[0],
            'data' => json_decode(end($tmp_arr), true)
        ];
    }
    
    file_put_contents('./result/' . $file, json_encode($data, JSON_UNESCAPED_UNICODE));
}

echo "======== done ========";
