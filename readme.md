处理抓取网络请求程序
1. 使用fiddler进行抓包
2. 保存抓包数据为txt文件
3. 将txt文件放置resource目录下
4. 运行处理程序  php processData.php
5. 结果保存在result目录下

微信小程序抓包教程：https://cloud.tencent.com/developer/article/1833882